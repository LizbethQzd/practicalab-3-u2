// components/Navbar.js
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";

import { getRol, cerrarSesion } from "@/hooks/conexion";

const getCookie = (name) => {
  const cookies = document.cookie.split(";");
  const cookie = cookies.find((c) => c.trim().startsWith(`${name}=`));
  if (!cookie) return null;
  return cookie.split("=")[1];
};

const Navbar = () => {
  const router = useRouter();
  const [role, setRole] = useState("");

  useEffect(() => {
    const obtenerRol = async () => {
      try {
        const data = await getRol();
        setRole(data);
      } catch (error) {
        console.error(error);
      }
    };
    obtenerRol();
  }, []);

  const handleLogout = async () => {
    try {
      const data = await cerrarSesion();
      console.log(data);
      router.push('/login');
    } catch (error) {
      console.error('Error al cerrar sesión:', error);
    }
  };

  return (
    <nav className="bg-gray-800 text-white p-4">
    <ul className="flex items-center justify-between">
      <li className="mr-4">{role}</li>
      <li
        className="cursor-pointer hover:text-gray-300"
        onClick={() => router.push("/books")}
      >
        <a>Inicio</a>
      </li>
      {role === "Gerente" && (
        <>
          <li
            className="cursor-pointer hover:text-gray-300"
            onClick={() => router.push("/books/subirlibro")}
          >
            <a>Subir libro</a>
          </li>
          <li
            className="cursor-pointer hover:text-gray-300"
            onClick={() => router.push("/vendedores")}
          >
            <a>Vendedores</a>
          </li>
          <li
            className="cursor-pointer hover:text-gray-300"
            onClick={() => router.push("/books/gestionarlibros")}
          >
            <a>Libros</a>
          </li>
        </>
      )}
      {role === "Agente vendedor" && (
        <>
          <li
            className="cursor-pointer hover:text-gray-300"
            onClick={() => router.push("/books/gestionarventas")}
          >
            <a>Ventas</a>
          </li>
          <li
            className="cursor-pointer hover:text-gray-300"
            onClick={() => router.push("/books/vendedor")}
          >
            <a>Libros vendedor</a>
          </li>
          <li
            className="cursor-pointer hover:text-gray-300"
            onClick={() => router.push("/books/ventaspendientes")}
          >
            <a>crear venta</a>
          </li>
          <li
            className="cursor-pointer hover:text-gray-300"
            onClick={() => router.push("/books/quincena")}
          >
            <a>Quincena</a>
          </li>
        </>
      )}
      {role === "Comprador" && (
        <>
          <li
            className="cursor-pointer hover:text-gray-300"
            onClick={() => router.push("/books/comprados")}
          >
            <a>Libros comprados</a>
          </li>
        </>
      )}
      <li
        className="cursor-pointer hover:text-gray-300"
        onClick={handleLogout}
      >
        <a>Cerrar Sesión</a>
      </li>
    </ul>
  </nav>
  
  );
};

export default Navbar;
