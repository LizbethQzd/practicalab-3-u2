import { NextResponse } from "next/server";
import { jwtVerify } from "jose";

export async function middleware(request) {
  try {
    let cookie = request.cookies.get("mytoken");
    console.log(cookie);
    if (!cookie) {
      return NextResponse.redirect(new URL("/login", request.nextUrl));
    }

    // Verificar el token
    const key = new TextEncoder().encode(process.env.ACCESS_TOKEN);
    const tokenVerification = await jwtVerify(cookie.value, key, {
      algorithms: ["HS256"],
    });

    const payload = tokenVerification.payload;
    // Verificar el rol del usuario y los permisos de acceso a las rutas
    const role = payload.info.role;
    console.log(role);

    if (role === "Gerente") {
      // Permitir acceso a rutas específicas para el rol de vendedor
      if (
        request.nextUrl.pathname === "/books" ||
        request.nextUrl.pathname === "/books/subirlibro" ||
        request.nextUrl.pathname === "/vendedores" ||
        request.nextUrl.pathname === "/books/gestionarlibros" ||
        request.nextUrl.pathname === "/books/gestionarlibros/editar/:external_id" 
      ) {
        return NextResponse.next();
      }
    } 
    if (role === "Comprador") {
      // Permitir acceso a rutas específicas para el rol de agente vendedor
      console.log(request.nextUrl);
      if (request.nextUrl.pathname === "/books" ||
          request.nextUrl.pathname === "/books/comprados"
      ) {
        return NextResponse.next();
      }
    }
    if (role === "Agente vendedor") {
      // Permitir acceso a rutas específicas para el rol de gerente
      if (
        request.nextUrl.pathname === "/books" ||
        request.nextUrl.pathname === "/books/gestionarventas" ||
        request.nextUrl.pathname === "/books/gestionarventas/editar/:external_id" ||
        request.nextUrl.pathname === "/books/vendedor" ||
        request.nextUrl.pathname === "/books/ventaspendientes" ||
        request.nextUrl.pathname === "/books/venta/:external_id" ||
        request.nextUrl.pathname === "/books/quincena"
      ) {
        return NextResponse.next();
      }
    }

    // Acceso no autorizado
    return NextResponse.error(new Error("Acceso no autorizado"), 403);
  } catch (error) {
    console.error(error);
    return NextResponse.error(new Error("Token inválido"), 401);
  }
}

export const config = {
  matcher: 
  [
    "/books",
    "/books/gestionarlibros",
    "/books/gestionarlibros/editar/:external_id",
    "/vendedores",
    "/books/subirlibro",
    "/books/comprados",
    "/books/gestionarventas",
    "/books/gestionarventas/editar/:external_id",
    "/books/vendedor",
    "/books/ventaspendientes",
    "/books/venta/:external_id",
    "/books/quincena",
  ],
};
