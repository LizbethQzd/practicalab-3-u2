const apiUrl = "http://localhost:9000";

export const getRol = async () => {
  try {
    const response = await fetch(`${apiUrl}/obtenerRol`, {
      credentials: "include",
    });
    const data = await response.json();
    return data.role;
  } catch (error) {
    console.error("Error al obtener el rol:", error);
    throw error;
  }
};

export const cerrarSesion = async () => {
  try {
    const response = await fetch(`${apiUrl}/cerrarsesion`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      credentials: "include",
    });
    const data = await response.json();
    return data;
  } catch (error) {
    console.error("Error al cerrar sesión:", error);
    throw error;
  }
};

export const registrarVendedor = async (info) => {
    try {
      const response = await fetch(`${apiUrl}/register/vendedores`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(info),
        credentials: "include",
      });
  
      const data = await response.json();
      return { success: response.ok, data };
    } catch (error) {
      console.error("Error al registrar vendedor:", error);
      throw error;
    }
  };

  export const registrarUsuario = async (info) => {
    try {
      const response = await fetch(`${apiUrl}/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(info),
        credentials: "include",
      });
  
      const data = await response.json();
      return { success: response.ok, data };
    } catch (error) {
      console.error("Error al registrar usuario:", error);
      throw error;
    }
  };
  
  export const iniciarSesion = async (info) => {
    try {
      const response = await fetch(`${apiUrl}/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(info),
        credentials: "include",
      });
  
      const data = await response.json();
      return { success: response.ok, data };
    } catch (error) {
      console.error("Error al iniciar sesión:", error);
      throw error;
    }
  };
  
  export const obtenerLibros = async () => {
    try {
      const response = await fetch(`${apiUrl}/obtenerLibros`, {
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      }
    } catch (error) {
      console.error("Error al obtener libros:", error);
      throw error;
    }
  };

  export const comprarLibro = async (external_id) => {
    try {
      const response = await fetch(`${apiUrl}/comprarlibro`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ external_id }),
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      }
    } catch (error) {
      console.error("Error al comprar libro:", error);
      throw error;
    }
  };

  export const obtenerLibrosComprados = async () => {
    try {
      const response = await fetch(`${apiUrl}/librosComprados`, {
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al obtener los libros comprados");
        throw new Error("Error al obtener los libros comprados");
      }
    } catch (error) {
      console.error("Error al obtener los libros comprados:", error);
      throw error;
    }
  };

  export const listarLibros = async () => {
    try {
      const response = await fetch(`${apiUrl}/listarLibros`, {
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al listar libros:", response.statusText);
        throw new Error("Error al listar libros");
      }
    } catch (error) {
      console.error("Error al listar libros:", error);
      throw error;
    }
  };

  export const buscarLibros = async (nombreLibro) => {
    try {
      const response = await fetch(`${apiUrl}/buscarlibros`, {
        method: "POST",
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ nombreLibro }),
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al buscar libros:", response.statusText);
        throw new Error("Error al buscar libros");
      }
    } catch (error) {
      console.error("Error al buscar libros:", error);
      throw error;
    }
  };

  export const editarLibro = async (external_id, formData) => {
    try {
      const response = await fetch(`${apiUrl}/editarLibro/${external_id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al editar el libro:", response.statusText);
        throw new Error("Error al editar el libro");
      }
    } catch (error) {
      console.error("Error al editar el libro:", error);
      throw error;
    }
  };
  
  export const obtenerLibroParaEditar = async (external_id) => {
    try {
      const response = await fetch(`${apiUrl}/obtenerlibro/${external_id}`, {
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error(
          "Error al obtener el libro para editar:",
          response.statusText
        );
        throw new Error("Error al obtener el libro para editar");
      }
    } catch (error) {
      console.error("Error al obtener el libro para editar:", error);
      throw error;
    }
  };

  export const obtenerVendedores = async () => {
    try {
      const response = await fetch(`${apiUrl}/obtenervendedores`, {
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al obtener vendedores:", response.statusText);
        throw new Error("Error al obtener vendedores");
      }
    } catch (error) {
      console.error("Error al obtener vendedores:", error);
      throw error;
    }
  };
  
  export const subirImagen = async (imagenLibro) => {
    try {
      const formData = new FormData();
      formData.append("image", imagenLibro);
  
      const response = await fetch(`${apiUrl}/subirimagen`, {
        method: "POST",
        body: formData,
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al subir la imagen:", response.statusText);
        throw new Error("Error al subir la imagen");
      }
    } catch (error) {
      console.error("Error al subir la imagen:", error);
      throw error;
    }
  };
  
  export const subirAudio = async (audioLibro) => {
    try {
      const formData = new FormData();
      formData.append("audio", audioLibro);
  
      const response = await fetch(`${apiUrl}/subiraudio`, {
        method: "POST",
        body: formData,
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al subir el audio:", response.statusText);
        throw new Error("Error al subir el audio");
      }
    } catch (error) {
      console.error("Error al subir el audio:", error);
      throw error;
    }
  };
  
  export const subirLibro = async (libroInfo) => {
    try {
      const response = await fetch(`${apiUrl}/subirlibro`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(libroInfo),
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al subir el libro:", response.statusText);
        throw new Error("Error al subir el libro");
      }
    } catch (error) {
      console.error("Error al subir el libro:", error);
      throw error;
    }
  };

  export const obtenerLibrosVendedor = async () => {
    try {
      const response = await fetch(`${apiUrl}/librovendedor`, {
        credentials: 'include'
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error('Error al obtener los libros del vendedor:', response.statusText);
        throw new Error('Error al obtener los libros del vendedor');
      }
    } catch (error) {
      console.error('Error al obtener los libros del vendedor:', error);
      throw error;
    }
  };

  export const obtenerVentasPendientes = async () => {
    try {
      const response = await fetch(`${apiUrl}/ventaspendientes`, {
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al obtener las ventas pendientes:", response.statusText);
        throw new Error("Error al obtener las ventas pendientes");
      }
    } catch (error) {
      console.error("Error al obtener las ventas pendientes:", error);
      throw error;
    }
  };


  export const crearVenta = async (externalId, formData) => {
    try {
      const response = await fetch(`${apiUrl}/crearventa/${externalId}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
        credentials: 'include',
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error('Error al crear la venta:', response.statusText);
        throw new Error('Error al crear la venta');
      }
    } catch (error) {
      console.error('Error al crear la venta:', error);
      throw error;
    }
  };
  
  export const obtenerDatosCliente = async (externalId) => {
    try {
      const response = await fetch(`${apiUrl}/cliente/${externalId}`, {
        credentials: 'include',
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error('Error al obtener los detalles del cliente:', response.statusText);
        throw new Error('Error al obtener los detalles del cliente');
      }
    } catch (error) {
      console.error('Error al obtener los detalles del cliente:', error);
      throw error;
    }
  };


  export const listarVentas = async () => {
    try {
      const response = await fetch(`${apiUrl}/listarventas`, {
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al obtener la lista de ventas:", response.statusText);
        throw new Error("Error al obtener la lista de ventas");
      }
    } catch (error) {
      console.error("Error al obtener la lista de ventas:", error);
      throw error;
    }
  };
  
  export const buscarVentasPorLibro = async (nombreLibro) => {
    try {
      const response = await fetch(`${apiUrl}/buscarventas`, {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ nombreLibro }),
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al buscar ventas por nombre de libro:", response.statusText);
        throw new Error("Error al buscar ventas por nombre de libro");
      }
    } catch (error) {
      console.error("Error al buscar ventas por nombre de libro:", error);
      throw error;
    }
  };

  export const modificarVenta = async (externalId, formData) => {
    try {
      const response = await fetch(`${apiUrl}/modificarventa/${externalId}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al modificar la venta:", response.statusText);
        throw new Error("Error al modificar la venta");
      }
    } catch (error) {
      console.error("Error al modificar la venta:", error);
      throw error;
    }
  };
  
  export const obtenerDatosClienteE = async (externalId) => {
    try {
      const response = await fetch(`${apiUrl}/cliente/${externalId}`, {
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al obtener los detalles del cliente:", response.statusText);
        throw new Error("Error al obtener los detalles del cliente");
      }
    } catch (error) {
      console.error("Error al obtener los detalles del cliente:", error);
      throw error;
    }
  };
  
  export const obtenerDatosVenta = async (externalId) => {
    try {
      const response = await fetch(`${apiUrl}/obtenerdatosventa/${externalId}`, {
        credentials: "include",
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al obtener los detalles de la venta:", response.statusText);
        throw new Error("Error al obtener los detalles de la venta");
      }
    } catch (error) {
      console.error("Error al obtener los detalles de la venta:", error);
      throw error;
    }
  };

  export const obtenerInfoQuincena = async () => {
    try {
      const response = await fetch(`${apiUrl}/visualizarquincena`, {
        credentials: 'include',
      });
  
      if (response.ok) {
        const data = await response.json();
        return data;
      } else {
        console.error("Error al obtener la información de la quincena:", response.statusText);
        throw new Error("Error al obtener la información de la quincena");
      }
    } catch (error) {
      console.error("Error al obtener la información de la quincena:", error);
      throw error;
    }
  };