"use client";
import { useState, useEffect } from "react";
import { useRouter } from "next/navigation";
import Navbar from "@/components/navbar";
import { obtenerVendedores,
  subirImagen,
  subirAudio,
  subirLibro, } from "@/hooks/conexion";

const SubirLibro = () => {
  const router = useRouter()
  const [imagenLibro, setimagenLibro] = useState(null);
  const [audioLibro, setAudioLibro] = useState(null);
  const [vendedores, setVendedores] = useState([]);
  const [seleccionarVendedores, setSeleccionarVendedores] = useState('');

  useEffect(() => {
    const vendedores = async () => {
      try {

          const data = await obtenerVendedores();
          console.log(data);
          setVendedores(data);
       
      } catch (error) {
        console.error("Error:", error);
      }
    }
    vendedores();
  }, [])



  const handleSubirImagen = async () => {
    try {
      const formData = new FormData();
      formData.append("image", imagenLibro);


        const data = await subirImagen(imagenLibro)
        console.log("Subida exitosa:", data);
      
    } catch (error) {
      console.error("Error:", error);
    }
  };

  const handleSeleccionarImagen = (e) => {
    const imagenLibroSeleccionado = e.target.files[0];
    setimagenLibro(imagenLibroSeleccionado);
  };

  const handleSubirAudio = async () => {
    try {
      const formData = new FormData();
      formData.append("audio", audioLibro);


        const data = await subirAudio(audioLibro);
        console.log("Subida exitosa:", data);

    } catch (error) {
      console.error("Error:", error);
    }
  };

  const handleSelectChange = (e) => {
    setSeleccionarVendedores(e.target.value);
  };

  const handleSeleccionarAudio = (e) => {
    const audioLibroSeleccionado = e.target.files[0];
    setAudioLibro(audioLibroSeleccionado);
  };

  const [nombreLibro, setNombreLibro] = useState("");
  const [codigoLibro, setCodigoLibro] = useState("");
  const [detalleLibro, setDetalleLibro] = useState(0);
  const [precioLibro, setPrecioLibro] = useState(0);
  const [cantidadLibro, setCantidadLibro] = useState(0);
  const [tipoLibro, setTipoLibro] = useState("Libro");
  const [idPersona, setIdpersona] = useState(null);

  const handleSubirLibro = async (e) => {
    e.preventDefault();
    const Info = {
      nombre: nombreLibro,
      codigo: codigoLibro,
      detalle: detalleLibro,
      precio: precioLibro,
      cantidad: cantidadLibro,
      tipo_libro: tipoLibro,
      id_persona: seleccionarVendedores,
    };
  
    try {
     
        const data = await subirLibro(Info);
        console.log("Subida exitosa:", data);
        router.push("/books/gestionarlibros");
    } catch (error) {
      console.error("Error:", error);
    }
  };
  
  return (
    <>
    <Navbar />
 <div className="max-w-md mx-auto mt-10 p-6 bg-white rounded-md shadow-md">
    <h1 className="text-2xl font-semibold mb-4">Subir Libro</h1>
  
    <label className="block mb-4">
      Imagen del Libro:
      <input
        type="file"
        accept=".jpg,.png"
        onChange={handleSeleccionarImagen}
        className="mt-2"
      />
      <button
        onClick={handleSubirImagen}
        className="bg-blue-500 text-white px-4 py-2 rounded-md mt-2 hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-700"
      >
        Subir Imagen
      </button>
    </label>
  
    <label className="block mb-4">
      Tipo de Libro:
      <select
        onChange={(e) => setTipoLibro(e.target.value)}
        className="mt-2 p-2 w-full border rounded-md"
      >
        <option value="Libro">Libro</option>
        <option value="Audiolibro">Audiolibro</option>
      </select>
    </label>
  
    {tipoLibro === "Audiolibro" && (
      <label className="block mb-4">
        Imagen de Audio:
        <input
          type="file"
          accept=".mp3"
          onChange={handleSeleccionarAudio}
          className="mt-2"
        />
        <button
          onClick={handleSubirAudio}
          className="bg-blue-500 text-white px-4 py-2 rounded-md mt-2 hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-700"
        >
          Subir AudioLibro
        </button>
      </label>
    )}
  
    <label className="block mb-4">
      Nombre del Libro:
      <input
        type="text"
        onChange={(e) => setNombreLibro(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
    </label>
  
    <label className="block mb-4">
      Código del Libro:
      <input
        type="text"
        onChange={(e) => setCodigoLibro(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
    </label>
  
    <label className="block mb-4">
      Detalle del Libro:
      <input
        type="text"
        onChange={(e) => setDetalleLibro(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
    </label>
  
    <label className="block mb-4">
      Precio del Libro:
      <input
        type="number"
        onChange={(e) => setPrecioLibro(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
    </label>
  
    <label className="block mb-4">
      Cantidad de Libros:
      <input
        type="number"
        onChange={(e) => setCantidadLibro(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
    </label>
  
    <label className="block mb-4">
      Vendedor:
      <select
        value={seleccionarVendedores}
        onChange={handleSelectChange}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      >
        <option value="">Selecciona un vendedor</option>
        {Array.isArray(vendedores.datos) &&
          vendedores.datos.map((vendedor) => (
            <option key={vendedor.id} value={vendedor.id}>
              {vendedor.nombres} {vendedor.apellidos}
            </option>
          ))}
      </select>
    </label>
  
    <button
      onClick={handleSubirLibro}
      className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-700"
    >
      Subir Libro
    </button>
  </div>  
    </>
   
  );
};

export default SubirLibro;
