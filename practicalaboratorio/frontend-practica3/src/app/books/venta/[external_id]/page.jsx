'use client';

import { useParams, useRouter } from 'next/navigation'
import { useState, useEffect } from 'react';
import { crearVenta, obtenerDatosCliente } from '@/hooks/conexion';
import Navbar from '@/components/navbar';
const CrearVenta = () => {
  const router = useRouter()
  const { external_id } = useParams();
  const [datos, setDatos] = useState([]);

  const [formData, setFormData] = useState({
    tipo: '',
    nombre_compania: '',
    tema_compania: '',
    num_factura: '',
    precio: '',
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {

      const data = await crearVenta(external_id, formData);
      console.log(data);
      router.push('/books/ventaspendientes');

    } catch (error) {
      console.error('Error al crear la venta:', error);
    }
  };

  useEffect(() => {
    const datosClientes = async () => {
      try {
        console.log(external_id);

        const data = await obtenerDatosCliente(external_id);
        setDatos(data);
      } catch (error) {
        console.error('Error fetching client details:', error);
      }
    };

    datosClientes();
    if (external_id) {
      console.log('Parámetro de la URL:', external_id);
    }
  }, [external_id]);

  return (
    <>
      <Navbar />
      <div className="flex min-h-screen flex-col items-center justify-between p-24 bg-gradient-to-b from-purple-200 to-indigo-200">
        
        <div className="mb-8">
          <h1 className="text-3xl font-bold mb-4 text-gray-800">Detalles del Comprador</h1>
          {datos.persona ? (
            <>
              <p className="text-gray-700">Nombre: {datos.persona.nombres}</p>
              <p className="text-gray-700">Apellidos: {datos.persona.apellidos}</p>
              <p className="text-gray-700">Dirección: {datos.persona.direccion}</p>
              <p className="text-gray-700">Celular: {datos.persona.celular}</p>
            </>
          ) : (
            <p className="text-gray-700">Cargando datos del comprador...</p>
          )}
        </div>

        <div>
          <h1 className="text-3xl font-bold mb-4 text-gray-800">Crear Venta</h1>
          <form onSubmit={handleSubmit} className="space-y-4">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
              <label className="block">
                <span className="text-gray-700">Tipo:</span>
                <input
                  type="text"
                  name="tipo"
                  value={formData.tipo}
                  onChange={handleInputChange}
                  className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
                />
              </label>

              <label className="block">
                <span className="text-gray-700">Nombre Compañía:</span>
                <input
                  type="text"
                  name="nombre_compania"
                  value={formData.nombre_compania}
                  onChange={handleInputChange}
                  className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
                />
              </label>

              <label className="block">
                <span className="text-gray-700">Tema Compañía:</span>
                <input
                  type="text"
                  name="tema_compania"
                  value={formData.tema_compania}
                  onChange={handleInputChange}
                  className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
                />
              </label>

              <label className="block">
                <span className="text-gray-700">Número de Factura:</span>
                <input
                  type="text"
                  name="num_factura"
                  value={formData.num_factura}
                  onChange={handleInputChange}
                  className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
                />
              </label>

              <label className="block">
                <span className="text-gray-700">Precio:</span>
                <input
                  type="text"
                  name="precio"
                  value={formData.precio}
                  onChange={handleInputChange}
                  className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
                />
              </label>
            </div>

            <button
              type="submit"
              className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-700"
            >
              Crear Venta
            </button>
          </form>
        </div>
      </div>


    </>
  );
};

export default CrearVenta;