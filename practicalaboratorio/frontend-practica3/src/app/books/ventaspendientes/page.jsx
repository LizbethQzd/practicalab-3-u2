"use client";
import Link from "next/link";
import Navbar from "@/components/navbar";
import React, { useEffect, useState } from "react";
import { obtenerVentasPendientes } from "@/hooks/conexion";

const VentasPendientes = () => {
  const [pendingSales, setPendingSales] = useState([]);

  useEffect(() => {
    const fetchPendingSales = async () => {
      try {

        const data = await obtenerVentasPendientes();
        console.log(data);
        setPendingSales(data);
      } catch (error) {
        console.error("Error fetching pending sales:", error);
      }
    };

    fetchPendingSales();
  }, []);

  return (
    <>
      <Navbar />
      <div className="max-w-3xl mx-auto mt-10 p-6 bg-white rounded-md shadow-md">
          <h1 className="text-2xl font-semibold mb-4">Crear Compra Pendiente</h1>
          <table className="w-full border-collapse border border-gray-300">
            <thead>
              <tr className="bg-gray-200">
                <th className="py-2 px-4 border">Cantidad</th>
                <th className="py-2 px-4 border">Nombre del libro</th>
                <th className="py-2 px-4 border">Opción</th>
              </tr>
            </thead>
            <tbody>
              {Array.isArray(pendingSales.detalles) &&
                pendingSales.detalles.map((detalle) => (
                  <tr key={detalle.external_id} className="border-b border-gray-300">
                    <td className="py-2 px-4 border">{detalle.cantidad}</td>
                    <td className="py-2 px-4 border">
                      {pendingSales.libros.find(
                        (libro) => libro.id === detalle.id_libro
                      )?.nombre || "N/A"}
                    </td>
                    <td className="py-2 px-4 border">
                      <Link
                        href={`/books/venta/${detalle.external_id}`}
                        className="text-blue-500 hover:underline"
                      >
                        Comprar
                      </Link>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
      </div>

    </>
  );
};

export default VentasPendientes;
