"use client";
import { useEffect, useState } from 'react';
import { useParams, useRouter } from 'next/navigation'
import Navbar from '@/components/navbar';
import { obtenerLibrosComprados } from '@/hooks/conexion';

const LibrosComprados = () => {
    const router = useRouter();
    const [librosComprados, setLibrosComprados] = useState([]);
    const [loading, setLoading] = useState(true);
  
    useEffect(() => {
      const fetchLibrosComprados = async () => {
        try {
            const data = await obtenerLibrosComprados();
            console.log(data);
            setLibrosComprados(data);
        } catch (error) {
          console.error('Error al obtener los libros comprados:', error);
          setLoading(false);
        }
      };
  
      fetchLibrosComprados();
    }, []);
  
    return (
      <>

      <Navbar />

      <div className="max-w-3xl mx-auto mt-10 p-6 bg-white rounded-md shadow-md">
      {librosComprados.map((libro) => (
        <div key={libro.external_id} className="mb-8 p-4 border border-gray-300 rounded-md">
          <h2 className="text-2xl font-semibold mb-2">{libro.nombre}</h2>
          <p>Código: {libro.codigo}</p>
          <p>Cantidad: {libro.cantidad}</p>
          <p>Precio: {libro.precio}</p>
    
          <img
            src={libro.imagen}
            height={300}
            width={150}
            alt={`Imagen de ${libro.nombre}`}
            className="my-4"
          />
    
          {libro.tipo_libro === 'AudioLibro' && (
            <audio controls className="my-4">
              <source src={libro.audio} type="audio/mp3" />
              Tu navegador no soporta la etiqueta de audio.
            </audio>
          )}
    
          <p className="mb-2">Detalle: {libro.detalle}</p>
        </div>
      ))}
    </div>
    </>
    );
  };
  
  export default LibrosComprados;
  