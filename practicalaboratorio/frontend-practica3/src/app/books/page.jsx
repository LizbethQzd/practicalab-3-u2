"use client";

import { useRouter } from "next/navigation";
import { useState, useEffect } from "react";
import Navbar from "@/components/navbar";
import { obtenerLibros, comprarLibro } from "@/hooks/conexion";
const Principal = () => {
  // Utiliza el hook dentro del componente funcional
  const router = useRouter();

  const [libros, setLibros] = useState([]);
  const obtenerInfo = async () => {
    try {
      const data = await obtenerLibros();
      setLibros(data);
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    obtenerInfo();
  }, []);

  const handleComprarLibro = async (external_id) => {
    try {
        const data = await comprarLibro(external_id);
        obtenerInfo();
        console.log(data);
    } catch (error) {
      console.error(error);
    }
    console.log(external_id);
  };

  return (
    <>
      <Navbar />
      <div className="max-w-3xl mx-auto mt-10 p-6 bg-white rounded-md shadow-md">
        <h1 className="text-2xl font-semibold mb-4">Lista de Libros</h1>
        <ul>
          {libros.map((libro, index) => (
            <li
              key={index}
              className="mb-6 p-4 border border-gray-300 rounded-md"
            >
              <p className="text-xl font-semibold">Nombre: {libro.nombre}</p>
              <p className="mb-2">Detalle: {libro.detalle}</p>
              {libro.imagen && (
                <img
                  src={libro.imagen}
                  height={300}
                  width={150}
                  alt={`Imagen de ${libro.nombre}`}
                  className="mb-2"
                />
              )}
              <p className="mb-2">Precio: {libro.precio}</p>
              <button
                onClick={() => handleComprarLibro(libro.external_id)}
                className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-700"
              >
                Comprar
              </button>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default Principal;
