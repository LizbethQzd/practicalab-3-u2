"use client";

import { editarLibro, obtenerLibroParaEditar } from "@/hooks/conexion";
import { useParams, useRouter } from "next/navigation";
import { useState, useEffect } from "react";

const Editarlibro = () => {
  const router = useRouter();
  const { external_id } = useParams();
  const [datos, setDatos] = useState([]);
  const [formData, setFormData] = useState({
    nombre: "",
    codigo: "",
    detalle: "",
    imagen: "",
    audio: "",
    precio: 0,
    cantidad: 0,
    tipo_libro: "",
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      
        const data = await editarLibro(external_id, formData);
        console.log(data);
        router.push("/books/gestionarlibros");
     
    } catch (error) {
      console.error("Error al editar el libro:", error);
    }
  };

  useEffect(() => {
    const obtenerLibro = async () => {
      try {
        
          const data = await obtenerLibroParaEditar(external_id);
          console.log(data);
          setFormData(data);
       
      } catch (error) {
        console.error("Error al obtener el libro para editar:", error);
      }
    };

    obtenerLibro();
  }, [external_id]);

  return (
    <>
      <div className="max-w-md mx-auto mt-10 p-6 bg-white rounded-md shadow-md">
        <h1 className="text-2xl font-semibold mb-4">Editar Libro</h1>
        <div>
          <form onSubmit={handleSubmit}>
            <label className="block mb-2">
              Nombre:
              <input
                type="text"
                name="nombre"
                value={formData.nombre}
                onChange={handleInputChange}
                className="mt-1 p-2 w-full border rounded-md"
              />
            </label>

            <label className="block mb-2">
              Código:
              <input
                type="text"
                name="codigo"
                value={formData.codigo}
                onChange={handleInputChange}
                className="mt-1 p-2 w-full border rounded-md"
              />
            </label>

            <label className="block mb-2">
              Detalle:
              <textarea
                name="detalle"
                value={formData.detalle}
                onChange={handleInputChange}
                className="mt-1 p-2 w-full border rounded-md"
              />
            </label>

            <label className="block mb-2">
              Imagen (URL):
              <input
                type="text"
                name="imagen"
                value={formData.imagen}
                onChange={handleInputChange}
                className="mt-1 p-2 w-full border rounded-md"
              />
            </label>

            <label className="block mb-2">
              Audio (URL):
              <input
                type="text"
                name="audio"
                value={formData.audio}
                onChange={handleInputChange}
                className="mt-1 p-2 w-full border rounded-md"
              />
            </label>

            <label className="block mb-2">
              Precio:
              <input
                type="number"
                name="precio"
                value={formData.precio}
                onChange={handleInputChange}
                className="mt-1 p-2 w-full border rounded-md"
              />
            </label>

            <label className="block mb-2">
              Cantidad:
              <input
                type="number"
                name="cantidad"
                value={formData.cantidad}
                onChange={handleInputChange}
                className="mt-1 p-2 w-full border rounded-md"
              />
            </label>

            <label className="block mb-2">
              Tipo de Libro:
              <input
                type="text"
                name="tipo_libro"
                value={formData.tipo_libro}
                onChange={handleInputChange}
                className="mt-1 p-2 w-full border rounded-md"
              />
            </label>

            <button
              type="submit"
              className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-700"
            >
              Guardar Libro
            </button>
          </form>
        </div>
      </div>
    </>
  );
};

export default Editarlibro;
