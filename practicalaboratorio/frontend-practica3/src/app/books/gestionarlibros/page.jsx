"use client";
import Link from "next/link";
import Navbar from "@/components/navbar";
import React, { useEffect, useState } from "react";
import { listarLibros, buscarLibros } from "@/hooks/conexion";

const Gestionarlibros = () => {
    const [libros, setlibros] = useState([]);
    const [nombreLibro, setNombreLibro] = useState('');
    
    const fetchlibros = async () => {
        try {
          
          const data = await listarLibros();
          console.log(data);
          setlibros(data);
        } catch (error) {
          console.error("Error fetching pending sales:", error);
        }
      };

    useEffect(() => {
      fetchlibros();
    }, []);
  
    const handleBuscarPorLibro = async () => {
      try {
        const data = await buscarLibros(nombreLibro)
        console.log(data);
        setlibros(data);
      } catch (error) {
        console.error("Error fetching sales by book name:", error);
      }
    };
  
    return (
      <>
      <Navbar />
      <div className="max-w-3xl mx-auto mt-10 p-6 bg-white rounded-md shadow-md">
      <h1 className="text-2xl font-semibold mb-4">Lista de Libros</h1>
    
      <table className="w-full border-collapse border border-gray-300">
        <thead>
          <tr className="bg-gray-200">
            <th className="py-2 px-4 border">Cantidad</th>
            <th className="py-2 px-4 border">Nombre del libro</th>
            <th className="py-2 px-4 border">Opción</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(libros) &&
            libros.map((l) => (
              <tr key={l.external_id} className="border-b border-gray-300">
                <td className="py-2 px-4 border">{l.cantidad}</td>
                <td className="py-2 px-4 border">{l.nombre}</td>
                <td className="py-2 px-4 border">
                  <Link
                    href={`/books/gestionarlibros/editar/${l.external_id}`}
                    className="text-blue-500 hover:underline"
                  >
                    Modificar
                  </Link>
                </td>
              </tr>
            ))}
        </tbody>
      </table>
    </div>    
      </>
    );
  };

export default Gestionarlibros;
