"use client";

import { useParams, useRouter } from "next/navigation";
import { useState, useEffect } from "react";
import Navbar from "@/components/navbar";
import { modificarVenta, obtenerDatosClienteE, obtenerDatosVenta } from "@/hooks/conexion";

const EditarVenta = () => {
  const router = useRouter();
  const { external_id } = useParams();
  const [datos, setDatos] = useState([]);

  const [formData, setFormData] = useState({
    tipo: "",
    nombre_compania: "",
    tema_compania: "",
    num_factura: "",
    precio: "",
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevData) => ({ ...prevData, [name]: value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const data = await modificarVenta(external_id, formData);
      console.log(data);
      router.push("/books/gestionarventas");

    } catch (error) {
      console.error("Error al crear la venta:", error);
    }
  };

  useEffect(() => {
    const datosClientes = async () => {
      try {
        console.log(external_id);

        const data = await obtenerDatosClienteE(external_id);
        setDatos(data);
      } catch (error) {
        console.error("Error fetching client details:", error);
      }
    };

    datosClientes();

    const datosVentas = async () => {
      try {
        console.log(external_id);

        const data = await obtenerDatosVenta(external_id);
        console.log(data);
        setFormData(data);
      } catch (error) {
        console.error("Error fetching client details:", error);
      }
    };

    datosVentas();
    if (external_id) {
      console.log("Parámetro de la URL:", external_id);
    }
  }, [external_id]);

  return (
    <>
      <Navbar />
      <div className="flex min-h-screen flex-col items-center justify-between p-24 bg-gradient-to-b from-purple-200 to-indigo-200">
      <form className="bg-white p-8 rounded-lg shadow-md max-w-md flex flex-col w-full">
        <div className="mb-8">
          <h1 className="text-2xl font-semibold mb-4">Detalles del Comprador</h1>
          {datos.persona ? (
            <>
              <p className="mb-2">Nombre: {datos.persona.nombres}</p>
              <p className="mb-2">Apellidos: {datos.persona.apellidos}</p>
              <p className="mb-2">Dirección: {datos.persona.direccion}</p>
              <p className="mb-2">Celular: {datos.persona.celular}</p>
            </>
          ) : (
            <p>Cargando datos del comprador...</p>
          )}

          <div className="mt-8">
            <h1 className="text-2xl font-semibold mb-4">Crear Venta</h1>
            <form onSubmit={handleSubmit} className="space-y-4">
              <label className="block">
                Tipo:
                <input
                  type="text"
                  name="tipo"
                  value={formData.tipo}
                  onChange={handleInputChange}
                  className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
                />
              </label>

              <label className="block">
                Nombre Compañía:
                <input
                  type="text"
                  name="nombre_compania"
                  value={formData.nombre_compania}
                  onChange={handleInputChange}
                  className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
                />
              </label>

              <label className="block">
                Tema Compañía:
                <input
                  type="text"
                  name="tema_compania"
                  value={formData.tema_compania}
                  onChange={handleInputChange}
                  className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
                />
              </label>

              <label className="block">
                Número de Factura:
                <input
                  type="text"
                  name="num_factura"
                  value={formData.num_factura}
                  onChange={handleInputChange}
                  className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
                />
              </label>

              <label className="block">
                Precio:
                <input
                  type="text"
                  name="precio"
                  value={formData.precio}
                  onChange={handleInputChange}
                  className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
                />
              </label>

              <button
                type="submit"
                className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-700"
              >
                Guardar Venta
              </button>
            </form>
          </div>
        </div>
        </form>  
      </div>
     
    </>
  );
};

export default EditarVenta;
