"use client";
import Link from "next/link";

import React, { useEffect, useState } from "react";
import Navbar from "@/components/navbar";
import { listarVentas, buscarVentasPorLibro } from "@/hooks/conexion";

const GestionarVentas = () => {
    const [ventas, setVentas] = useState([]);
    const [nombreLibro, setNombreLibro] = useState('');
    
    const fetchVentas = async () => {
        try {
          
          const data = await listarVentas();
          console.log(data);
          setVentas(data);
        } catch (error) {
          console.error("Error fetching pending sales:", error);
        }
      };

    useEffect(() => {
      fetchVentas();
    }, []);
  
    const handleBuscarPorLibro = async () => {
      try {
        
        const data = await buscarVentasPorLibro(nombreLibro)
        console.log(data);
        setVentas(data);
      } catch (error) {
        console.error("Error fetching sales by book name:", error);
      }
    };
  
    return (
     <>
      <Navbar />
     <div className="max-w-3xl mx-auto mt-10 p-6 bg-white rounded-md shadow-md">
  <h1 className="text-2xl font-semibold mb-4">Lista de Ventas</h1>
  
  <div className="mb-4">
    <label htmlFor="nombreLibro" className="mr-2">
      Nombre del Libro:
    </label>
    <input
      type="text"
      id="nombreLibro"
      value={nombreLibro}
      onChange={(e) => setNombreLibro(e.target.value)}
      className="p-2 border rounded-md"
    />
    <button
      onClick={handleBuscarPorLibro}
      className="bg-blue-500 text-white px-4 py-2 rounded-md ml-2 hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-700"
    >
      Buscar
    </button>
    <button
      onClick={fetchVentas}
      className="bg-gray-500 text-white px-4 py-2 rounded-md ml-2 hover:bg-gray-600 focus:outline-none focus:ring focus:border-gray-700"
    >
      Cancelar
    </button>
  </div>
  
  <table className="w-full border-collapse border border-gray-300">
    <thead>
      <tr className="bg-gray-200">
        <th className="py-2 px-4 border">Cantidad</th>
        <th className="py-2 px-4 border">Nombre del libro</th>
        <th className="py-2 px-4 border">Opción</th>
      </tr>
    </thead>
    <tbody>
      {Array.isArray(ventas.detalles) &&
        ventas.detalles.map((detalle) => (
          <tr key={detalle.external_id} className="border-b border-gray-300">
            <td className="py-2 px-4 border">{detalle.cantidad}</td>
            <td className="py-2 px-4 border">
              {ventas.libros.find(
                (libro) => libro.id === detalle.id_libro
              )?.nombre || "N/A"}
            </td>
            <td className="py-2 px-4 border">
              <Link
                href={`/books/gestionarventas/editar/${detalle.external_id}`}
                className="text-blue-500 hover:underline"
              >
                Modificar
              </Link>
            </td>
          </tr>
        ))}
    </tbody>
  </table>
</div>

     </>
    );
  };

export default GestionarVentas;
