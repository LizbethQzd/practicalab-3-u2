"use client";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { iniciarSesion } from "@/hooks/conexion";
import Image from 'next/image'
import Logo from "../../../public/logo.png";
import mensajes from "@/components/Mensajes";

const Login = () => {
  const router = useRouter();

  const [nombreUsuario, setNombreUsuario] = useState("");
  const [clave, setClave] = useState("");
  const [IniciarSessionExitoso, setIniciarSessionExitoso] = useState(false);

  const handleIniciarSession = async (e) => {
    e.preventDefault();
    const Info = {
      nombre_usuario: nombreUsuario,
      clave: clave,
    };

    try {
      const { success, data } = await iniciarSesion(Info);

      console.log(data);

      if (success && data.nombreUsuario) {
        localStorage.setItem("token", data.token);
        setIniciarSessionExitoso(true);
        mensajes("EXITO", "Informacion", "success")
        router.push("/books");
      } else {
        setIniciarSessionExitoso(false);
        mensajes("No se pudo iniciar sesion", "Error", "error")
        
      }
    } catch (error) {
      console.error("Error al procesar inicio de sesión:", error);
      // Manejar el error según sea necesario
    }
  };
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24 bg-gradient-to-b from-purple-200 to-indigo-200">
      
        <form className="bg-white rounded-lg shadow-md p-8">
          <div className="mb-4">
            <div className="mb-8 text-center">
              <Image src={Logo} alt="Logo" height={300} width={200} className="h-24 w-24 mx-auto mb-4" />
              <h2 className="text-xl font-bold text-gray-800">Iniciar Sesión</h2>
            </div>
            <label
              htmlFor="nombreUsuario"
              className="text-sm font-medium text-gray-600"
            >
              Nombre de usuario:
            </label>
            <input
              id="nombreUsuario"
              type="text"
              onChange={(e) => setNombreUsuario(e.target.value)}
              className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
            />
          </div>
          <div className="mb-4">
            <label htmlFor="clave" className="text-sm font-medium text-gray-600">
              Clave:
            </label>
            <input
              id="clave"
              type="password"
              onChange={(e) => setClave(e.target.value)}
              className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
            />
          </div>
          <div className="text-center">
            <button
              className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-700"
              onClick={handleIniciarSession}
            >
              Iniciar Sesión
            </button>
          </div>
        </form>
      
    </main>
  );


};

export default Login;
