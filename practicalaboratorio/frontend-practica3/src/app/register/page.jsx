'use client';
import { useRouter } from 'next/navigation';
import { useState } from "react";
import { registrarUsuario } from '@/hooks/conexion';
import Image from 'next/image'
import Logo from "../../../public/logo.png";

const Register = () => {

  const router = useRouter()

  const [nombres, setNombres] = useState("");
  const [apellidos, setApellidos] = useState("");
  const [ruc_ci, setRuc_ci] = useState("");
  const [direccion, setDireccion] = useState("");
  const [celular, setCelular] = useState("");
  const [fecha_nacimiento, setFecha_nacimiento] = useState("");
  const [nombreUsuario, setNombreUsuario] = useState("");
  const [correo, setCorreo] = useState("");
  const [clave, setClave] = useState("");
  const [RegistroExitoso, setRegistroExitoso] = useState(false);

  const handleRegistro = async (e) => {
    e.preventDefault();
    const Info = {
      nombres: nombres,
      apellidos: apellidos,
      ruc_ci: ruc_ci,
      direccion: direccion,
      celular: celular,
      fecha_nacimiento: fecha_nacimiento,
      correo: correo,
      nombre_usuario: nombreUsuario,
      clave: clave,
    };

    try {
      const { success, data } = await registrarUsuario(Info);
      console.log(data)
      if (success) {
        setRegistroExitoso(true);
        router.push("/login");
      } else {
        setRegistroExitoso(false);
      }
    } catch (error) {
      console.error("Error al registrar usuario:", error);
      throw error;
    }
  };

  return (
    <main className="flex min-h-screen items-center justify-center bg-gradient-to-b from-purple-200 to-indigo-200">
      <form className="bg-white p-8 rounded-lg shadow-md max-w-md flex flex-col w-full">
        <div className="mb-8 text-center">
          <Image src={Logo} alt="Logo" height={300} width={200} className="h-24 w-24 mx-auto mb-4" />
          <h2 className="text-xl font-bold text-gray-800">Registro de Clientes</h2>
        </div>
        <div className="flex flex-wrap -mx-4">
          <div className="w-full md:w-1/2 px-4">
            <label htmlFor="nombres" className="text-sm font-medium text-gray-600">
              Nombres:
            </label>
            <input
              id="nombres"
              type="text"
              onChange={(e) => setNombres(e.target.value)}
              className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
            />

            <label htmlFor="apellidos" className="mt-4 text-sm font-medium text-gray-600">
              Apellidos:
            </label>
            <input
              id="apellidos"
              type="text"
              onChange={(e) => setApellidos(e.target.value)}
              className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
            />
            <label htmlFor="rucCi" className="mt-4 text-sm font-medium text-gray-600">
              RUC/CI:
            </label>
            <input
              id="rucCi"
              type="text"
              onChange={(e) => setRuc_ci(e.target.value)}
              className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
            />

            <label htmlFor="direccion" className="mt-4 text-sm font-medium text-gray-600">
              Dirección:
            </label>
            <input
              id="direccion"
              type="text"
              onChange={(e) => setDireccion(e.target.value)}
              className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
            />

            <label htmlFor="celular" className="mt-4 text-sm font-medium text-gray-600">
              Celular:
            </label>
            <input
              id="celular"
              type="text"
              onChange={(e) => setCelular(e.target.value)}
              className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
            />

          </div>
          <div className="w-full md:w-1/2 px-4">
            {/* Campos para la segunda columna */}
            <label htmlFor="fechaNacimiento" className="mt-4 text-sm font-medium text-gray-600">
              Fecha de nacimiento:
            </label>
            <input
              id="fechaNacimiento"
              type="date"
              onChange={(e) => setFecha_nacimiento(e.target.value)}
              className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
            />

            <label htmlFor="nombreUsuario" className="mt-4 text-sm font-medium text-gray-600">
              Nombre de usuario:
            </label>
            <input
              id="nombreUsuario"
              type="text"
              onChange={(e) => setNombreUsuario(e.target.value)}
              className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
            />

            <label htmlFor="correo" className="mt-4 text-sm font-medium text-gray-600">
              Correo:
            </label>
            <input
              id="correo"
              type="text"
              onChange={(e) => setCorreo(e.target.value)}
              className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
            />

            <label htmlFor="clave" className="mt-4 text-sm font-medium text-gray-600">
              Clave:
            </label>
            <input
              id="clave"
              type="password"
              onChange={(e) => setClave(e.target.value)}
              className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
            />
          </div>
        </div>

        <button
          className="mt-6 bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-700"
          onClick={handleRegistro}
        >
          Registrar
        </button>
      </form>
    </main>
  );

};

export default Register;
