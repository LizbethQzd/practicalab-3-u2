'use client';
import { useRouter } from 'next/navigation';
import { useState } from "react";
import Navbar from '@/components/navbar';

import { registrarVendedor } from '@/hooks/conexion';

const Register = () => {

  const router = useRouter()

  const [nombres, setNombres] = useState("");
  const [apellidos, setApellidos] = useState("");
  const [ruc_ci, setRuc_ci] = useState("");
  const [direccion, setDireccion] = useState("");
  const [celular, setCelular] = useState("");
  const [fecha_nacimiento, setFecha_nacimiento] = useState("");
  const [nombreUsuario, setNombreUsuario] = useState("");
  const [correo, setCorreo] = useState("");
  const [clave, setClave] = useState("");
  const [RegistroExitoso, setRegistroExitoso] = useState(false);

  const handleRegistro = async (e) => {
    e.preventDefault();
    const Info = {
      nombres: nombres,
      apellidos: apellidos,
      ruc_ci: ruc_ci,
      direccion: direccion,
      celular: celular,
      fecha_nacimiento: fecha_nacimiento,
      correo: correo,
      nombre_usuario: nombreUsuario,
      clave: clave,
    };

    try {
      const { success, data } = await registrarVendedor(Info);
      console.log(data)
      if (success) {
        router.push('/books');
        setRegistroExitoso(true);
      } else {
        setRegistroExitoso(false);
      }
    } catch (error) {
      console.error("Error al registrar vendedor:", error);
      throw error;
    }
    
  };

  return (
    <>
    <Navbar />
    
    <main className="flex min-h-screen flex-col items-center justify-between p-24 bg-gradient-to-b from-purple-200 to-indigo-200">
    <form className="bg-white p-8 rounded-lg shadow-md max-w-md">
      {RegistroExitoso && (
        <div className="bg-green-100 text-green-800 p-4 mb-4">
          Registro Exitoso
        </div>
      )}
  
      <label className="text-xl font-semibold mb-4">REGISTRO DE VENDEDORES:</label>
  
      <label className="block mb-2">Nombres:</label>
      <input
        type="text"
        onChange={(e) => setNombres(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
  
      <label className="block mb-2">Apellidos:</label>
      <input
        type="text"
        onChange={(e) => setApellidos(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
  
      <label className="block mb-2">RUC/CI:</label>
      <input
        type="text"
        onChange={(e) => setRuc_ci(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
  
      <label className="block mb-2">Direccion:</label>
      <input
        type="text"
        onChange={(e) => setDireccion(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
  
      <label className="block mb-2">Celular:</label>
      <input
        type="text"
        onChange={(e) => setCelular(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
  
      <label className="block mb-2">Fecha de nacimiento:</label>
      <input
        type="date"
        onChange={(e) => setFecha_nacimiento(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
  
      <label className="block mb-2">Nombre de usuario:</label>
      <input
        type="text"
        onChange={(e) => setNombreUsuario(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
  
      <label className="block mb-2">Correo:</label>
      <input
        type="text"
        onChange={(e) => setCorreo(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
  
      <label className="block mb-2">Clave:</label>
      <input
        type="password"
        onChange={(e) => setClave(e.target.value)}
        className="mt-1 p-2 w-full border rounded-md focus:outline-none focus:ring focus:border-blue-500 border-gray-400"
      />
  
      <button
        className="bg-blue-500 text-white px-4 py-2 rounded-md mt-4 hover:bg-blue-600 focus:outline-none focus:ring focus:border-blue-700"
        onClick={handleRegistro}
      >
        Registrar
      </button>
    </form>
  </main>
  </>
  );
};

export default Register;
