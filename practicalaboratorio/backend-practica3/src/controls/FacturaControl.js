const Factura = require('../models/factura');

class FacturaControl {
    async crear_factura(req, res) {
        const factura = new Factura(req.body);
        await factura.save();
        res.status(201).json(factura);
    }

    async obtener_facturas(req, res) {
        const factura = await Factura.find();
        res.status(200).json(factura);
    }

    async obtener_factura(req, res) {
        const factura = await Factura.findById(req.params.id);
        res.status(200).json(factura);
    }

    async actualizar_factura(req, res) {
        const factura = await Factura.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.status(200).json(factura);
    }

    async eliminar_factura(req, res) {
        const factura = await Factura.findByIdAndDelete(req.params.id);
        res.status(200).json(factura);
    }
}

module.exports = FacturaControl;