const Libro = require("../models/libro");
const Cuenta = require("../models/cuenta");
const Factura = require("../models/factura");
const Detalle = require("../models/detalle");
const { Op } = require('sequelize');
var jwt = require('jsonwebtoken');
const cloudinary = require("../config/cloudinary");
var uuid = require('uuid');
require("dotenv").config();

let imgUrl;
let audioUrl;


class LibroControl {

  async subirImagen(req, res) {
    try {
      const resp = await cloudinary.uploader.upload(req.file.path);
      res.status(200).json({
        msg: "Subida exitosa",
        data: resp,
      });
      console.log(resp.secure_url);
      imgUrl = resp.secure_url;
    } catch (err) {
      res.status(500).json({ err: err });
      console.error(err);
    }
  }

  async subirAudio(req, res) {
    try {
      const resp = await cloudinary.uploader.upload(
        req.file.path,
        function (result) {
          console.log(result);
        },
        { resource_type: "video" }
      );
      const resp2 = await resp;

      audioUrl = resp2?.secure_url;
      res.status(200).json({
        msg: "Subida exitosa",
        data: resp,
      });
      
      console.log(audioUrl);
    } catch (err) {
      res.status(500).json({ err: err.message });
      console.error(err);
    }
  }

  async subir_libro(req, res) {
    try {
      const { nombre, codigo, detalle, precio, tipo_libro, cantidad, id_persona } = req.body;
      const decoded = jwt.verify(req.cookies?.mytoken, process.env.ACCESS_TOKEN);
      console.log("este es el decoded: " + JSON.stringify(decoded));
      console.log(decoded.info.external);
      const cuenta = await Cuenta.findOne({ where: { external_id: decoded.info.external } });
      if (!cuenta) return res.status(404).json({ msg: 'No existe la cuenta'});
        if (req.body.tipo_libro === "Audiolibro") {
          const libro = await Libro.create({
            nombre: nombre,
            codigo: codigo,
            detalle: detalle,
            imagen: imgUrl,
            audio: audioUrl,
            precio: precio,
            tipo_libro: tipo_libro,
            cantidad: cantidad,
            id_persona: id_persona,
            external_id: uuid.v4(),
          });
          return res.status(200).json(libro);
        } else {
          const libro = await Libro.create({
            nombre: nombre,
            codigo: codigo,
            detalle: detalle,
            imagen: imgUrl,
            precio: precio,
            tipo_libro: tipo_libro,
            cantidad: cantidad,
            id_persona: id_persona,
            external_id: uuid.v4(),
          });
          return res.status(200).json(libro);
        }
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Error al subir el libro" });
    }
  }

  async obtener_libros(req, res) {
    try {
        const token = req.cookies?.mytoken;

        if (!token) {
            return res.status(401).json({ error: "Token no proporcionado" });
        }

        console.log('este es el token');
        console.log(token);

        const decoded = jwt.verify(token, process.env.ACCESS_TOKEN);
        console.log("este es el decoded: " + JSON.stringify(decoded));

        const cuenta = await Cuenta.findOne({ where: { external_id: decoded.info.external } });
        if (!cuenta) return res.status(404).json({ msg: 'No existe la cuenta' });

        const facturas = await Factura.findAll({ where: { id_persona: cuenta.id_persona } });

        // Obtener todos los IDs de libros comprados
        const librosCompradosIds = (await Detalle.findAll({
            where: { id_factura: facturas.map(factura => factura.id) },
        })).map(detalle => detalle.id_libro);


        // Obtener la lista de libros excluyendo los comprados por la persona
        const libros = await Libro.findAll({
            attributes: ['nombre', 'detalle', 'imagen', 'precio', 'tipo_libro', 'id_persona', 'external_id'],
            where: {
                id_persona: {
                    [Op.not]: cuenta.id_persona
                },
                id: {
                    [Op.notIn]: librosCompradosIds
                },
                cantidad: {
                  [Op.gt]: 0
                }
            },
        });

        res.status(200).json(libros);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Error al obtener los libros" });
    }
}

  async libros_comprados(req, res){
    try {
      const token = req.cookies?.mytoken;
      const decoded = jwt.verify(token, process.env.ACCESS_TOKEN);
      const cuenta = await Cuenta.findOne({ where: { external_id: decoded.info.external } });
      
      if (!cuenta) {
        return res.status(404).json({ msg: 'No existe la cuenta' });
      }
  
      const facturas = await Factura.findAll({ where: { id_persona: cuenta.id_persona } });
  
      if (!facturas || facturas.length === 0) {
        return res.status(404).json({ msg: 'No existen facturas' });
      }
  
      const detallesPromises = facturas.map(async (factura) => {
        const detalles = await Detalle.findAll({ where: { id_factura: factura.id } });
        return detalles;
      });
  
      const detallesArrays = await Promise.all(detallesPromises);
      const detalles = detallesArrays.flat();
  
      if (!detalles || detalles.length === 0) {
        return res.status(404).json({ msg: 'No existen detalles' });
      }
  
      const librosPromises = detalles.map(async (detalle) => {
        const libro = await Libro.findOne({ where: { id: detalle.id_libro } });
        return libro;
      });
  
      const libros = await Promise.all(librosPromises);
  
      if (!libros || libros.length === 0) {
        return res.status(404).json({ msg: 'No existen libros' });
      }
  
      res.status(200).json(libros);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Error al obtener los libros' });
    }
  }

  async libro_vendedor(req, res){
    try {
      const token = req.cookies?.mytoken;
      const decoded = jwt.verify(token, process.env.ACCESS_TOKEN);
      const cuenta = await Cuenta.findOne({ where: { external_id: decoded.info.external } });
      if (!cuenta) return res.status(404).json({ msg: 'No existe la cuenta' });
      //const libro = await Libro.findOne({ where: { id_persona: cuenta.id_persona } });

      const libros = await Libro.findAll({
        where: {
          id_persona: cuenta.id_persona
        }
      })
      
      res.status(200).json(libros);

    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Error al obtener los libros" });
    }
  }

  async editar_libro(req, res) {
    try {
      const { external_id } = req.params;
      const { nombre, codigo, detalle, imagen, audio, precio, cantidad, tipo_libro } = req.body;
      console.log(nombre, codigo, detalle, imagen, audio, precio, tipo_libro);
 // Buscar el libro por external_id
    const libro = await Libro.findOne({ where: { external_id } });

    // Verificar si el libro existe
    if (!libro) {
      return res.status(404).json({ error: 'Libro no encontrado' });
    }

    // Actualizar la información del libro
    libro.nombre = nombre;
    libro.codigo = codigo;
    libro.detalle = detalle;
    libro.imagen = imagen;
    libro.audio = audio;
    libro.precio = precio;
    libro.cantidad = cantidad;
    libro.tipo_libro = tipo_libro;

    // Guardar los cambios en la base de datos
    await libro.save();

    res.status(200).json({ mensaje: 'Libro actualizado exitosamente' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Error al actualizar el libro' });
  }
}

async listar_libros(req, res) {
  try {
    const libros = await Libro.findAll();
    res.status(200).json(libros);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Error al obtener los libros' });
  }
}

async obtener_libro_editar(req, res) {
  try {
    const { external_id } = req.params;

    // Buscar el libro por external_id
    const libro = await Libro.findOne({ where: { external_id } });

    // Verificar si el libro existe
    if (!libro) {
      return res.status(404).json({ error: 'Libro no encontrado' });
    }

    // Devolver los datos del libro
    res.status(200).json(libro);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Error al obtener los datos del libro' });
  }
}

async visualizar_quincena(req, res) {
  try {
    // Obtener la fecha de hace dos minutos
    const dosMinutosAtras = new Date(Date.now() - 2 * 60 * 1000);

    // Obtener todos los detalles que cumplen con la condición de fecha
    const detalles = await Detalle.findAll({
      where: {
        createdAt: {
          [Op.lt]: dosMinutosAtras,
        },
      },
    });

    if (!detalles || detalles.length === 0) {
      return res.status(404).json({ error: 'No existen detalles creados en los últimos dos minutos' });
    }

    // Obtener libros asociados a los detalles que han pasado dos minutos
    const libros = await Libro.findAll({
      where: {
        id: detalles.map((detalle) => detalle.id_libro),
      },
      attributes: ['nombre', 'codigo', 'detalle', 'imagen', 'audio', 'precio', 'tipo_libro'],
    });

    res.status(200).json(libros);

  } catch (error) {
    console.error('Error al obtener detalles:', error);
    res.status(500).json({ error: 'Error interno del servidor' });
  }
}


}

module.exports = LibroControl;
