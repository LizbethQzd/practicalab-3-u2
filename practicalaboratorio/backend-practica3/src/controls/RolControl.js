const Rol = require('../models/rol');

class RolControl {
    async crear_rol(req, res) {
        const rol = new Rol(req.body);
        await rol.save();
        res.status(201).json(rol);
    }

    async obtener_roles(req, res) {
        const rol = await Rol.find();
        res.status(200).json(rol);
    }

    async obtener_rol(req, res) {
        const rol = await Rol.findById(req.params.id);
        res.status(200).json(rol);
    }

    async actualizar_rol(req, res) {
        const rol = await Rol.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.status(200).json(rol);
    }

    async eliminar_rol(req, res) {
        const rol = await Rol.findByIdAndDelete(req.params.id);
        res.status(200).json(rol);
    }
}

module.exports = RolControl;