const Persona = require('../models/persona');

class PersonaControl {
    async crear_persona(req, res) {
        const persona = new Persona(req.body);
        await persona.save();
        res.status(201).json(persona);
    }

    async obtener_personas(req, res) {
        const persona = await Persona.find();
        res.status(200).json(persona);
    }

    async obtener_persona(req, res) {
        const persona = await Persona.findById(req.params.id);
        res.status(200).json(persona);
    }

    async actualizar_persona(req, res) {
        const persona = await Persona.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.status(200).json(persona);
    }

    async eliminar_persona(req, res) {
        const persona = await Persona.findByIdAndDelete(req.params.id);
        res.status(200).json(persona);
    }
}

module.exports = PersonaControl;