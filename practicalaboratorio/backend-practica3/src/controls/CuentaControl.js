const Cuenta = require("../models/cuenta");
const Persona = require("../models/persona");
const Rol = require("../models/rol");
var uuid = require("uuid");
const bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");
require("dotenv").config();

class CuentaControl {

  async registro_usuario(req, res) {
    try {
      const {
        nombres,
        apellidos,
        ruc_ci,
        direccion,
        celular,
        fecha_nacimiento,
        correo,
        nombre_usuario,
        clave,
      } = req.body;

      const claveCifrada = await bcrypt.hash(clave, 10);

      const rol = await Rol.findOne({ where: { tipo: "Comprador" } });
      const persona = await Persona.create({
        nombres: nombres,
        apellidos: apellidos,
        ruc_ci: ruc_ci,
        direccion: direccion,
        celular: celular,
        fecha_nacimiento: fecha_nacimiento,
        id_rol: rol.id,
        external_id: uuid.v4(),
      });

      const cuenta = await Cuenta.create({
        nombre_usuario: nombre_usuario,
        correo: correo,
        clave: claveCifrada,
        id_persona: persona.id,
        external_id: uuid.v4(),
      });
      const dat = {
        persona: persona,
        rol: rol,
        //persona_rol: persona_rol,
        cuenta: cuenta,
      };
      res.status(200).json({
        msq: "Usuario registrado con éxito",
        datos: {
          persona: persona,
          rol: rol,
          //persona_rol: persona_rol,
          cuenta: cuenta,
        },
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        msq: "No se pudo registrar el usuario",
        datos: err,
      });
    }
  }

  async obtener_rol(req, res) {
    try {
      const token = req.cookies?.mytoken;

      if (!token) {
        return res.status(401).json({ error: "Token no proporcionado" });
      }

      const decoded = jwt.verify(token, process.env.ACCESS_TOKEN);
      console.log("este es el decoded del rol: " + JSON.stringify(decoded));

      res.status(200).json({
        role: decoded.info.role
      });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Error al obtener los libros" });
    }
  }

  async registro_usuario_por_gerente(req, res) {
    try {
      const {
        nombres,
        apellidos,
        ruc_ci,
        direccion,
        celular,
        fecha_nacimiento,
        correo,
        nombre_usuario,
        clave,
      } = req.body;

      const claveCifrada = await bcrypt.hash(clave, 10);

      const rol = await Rol.findOne({ where: { tipo: "Agente vendedor" } });

      const persona = await Persona.create({
        nombres: nombres,
        apellidos: apellidos,
        ruc_ci: ruc_ci,
        direccion: direccion,
        celular: celular,
        fecha_nacimiento: fecha_nacimiento,
        id_rol: rol.id,
        external_id: uuid.v4(),
      });

      const cuenta = await Cuenta.create({
        nombre_usuario: nombre_usuario,
        correo: correo,
        clave: claveCifrada,
        id_persona: persona.id,
        external_id: uuid.v4(),
      });
      const dat = {
        persona: persona,
        rol: rol,
        //persona_rol: persona_rol,
        cuenta: cuenta,
      };
      console.log(dat);
      res.status(200).json({
        msq: "Usuario registrado con éxito",
        datos: {
          persona: persona,
          rol: rol,
          //persona_rol: persona_rol,
          cuenta: cuenta,
        },
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        msq: "No se pudo registrar el usuario",
        datos: err,
      });
    }
  }

  async inicio_sesion(req, res) {
    try {
      const { nombre_usuario, clave } = req.body;

      const nombreUsuario = await Cuenta.findOne({
        where: { nombre_usuario: nombre_usuario },
      });
      if (!nombreUsuario)
        return res.status(404).json({ msg: "No existe el usuario" });
      const claveCifrada = await bcrypt.compare(clave, nombreUsuario.clave);
      if (!claveCifrada)
        return res.status(404).json({ msg: "Clave incorrecta" });

      const persona = await Persona.findByPk(nombreUsuario.id_persona);
      if (!persona)
        return res.status(404).json({ msg: "No existe la persona" });
      console.log(persona);
      const rol = await Rol.findByPk(persona.id_rol);
      console.log(rol);
      if (!rol) return res.status(404).json({ msg: "No existe el rol" });

      const accesToken = jwt.sign(
        {
          info: {
            nombreUsuario: nombreUsuario.nombre_usuario,
            external: nombreUsuario.external_id,
            role: rol.tipo,
            check: true,
          },
        },
        process.env.ACCESS_TOKEN,
        { expiresIn: "30d" }
      );

      res.cookie("mytoken", accesToken, {
        httpOnly: true,
        secure: true,
        sameSite: "none",
        maxAge: 1000 * 60 * 60 * 24 * 30,
      });

      res.status(200).json({
        msq: "Usuario iniciado sesion",
        nombreUsuario: nombreUsuario.nombre_usuario,
        token: accesToken,
        role: rol.tipo,
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({
        msq: "No se pudo iniciar sesion",
        datos: err,
      });
    }
  }

  async obtener_vendedores(req, res) {
    try {
        const rol = await Rol.findOne({ where: { tipo: 'Agente vendedor' } })

        const personas = await Persona.findAll({ where: { id_rol: rol.id } })

        return res.status(200).json({
            msq: "Vendedores encontrados",
            datos: personas
        })
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: "Error al obtener los libros" });
    }
  }

  async obtener_perfil(req, res) {
    try {
      const mycookie = req.cookies?.mytoken;

      if (!mycookie) {
        return res.status(401).json({ error: "No hay token proporcionado" });
      }

      const decoded = jwt.verify(mycookie, process.env.ACCESS_TOKEN);
      console.log(decoded);
      return res.status(200).json({
        msq: "Usuario iniciado sesion",
        datos: decoded,
      });
    } catch (error) {
      console.error(error);
      return res.status(401).json({ error: "Token inválido" });
    }
  }

  async cerrar_session(req, res) {
    try {
      const mycookie = req.cookies?.mytoken;
      if (!mycookie) {
        return res.status(401).json({ error: "No hay token proporcionado" });
      }

      const decoded = jwt.verify(mycookie, process.env.ACCESS_TOKEN);

      res.clearCookie("mytoken", {
        httpOnly: true,
        secure: true,
        sameSite: "none",
        maxAge: 0,
      });

      return res.status(200).json({
        msq: "Usuario cerrado sesion",
        datos: decoded,
      });
    } catch (error) {
      console.error(error);
      return res.status(401).json({ error: "Token inválido" });
    }
  }

  async obtener_cuentas(req, res) {
    try {
      const cuenta = await Cuenta.findAll();
      res.status(200).json(cuenta);
    } catch (err) {
      console.log(err);
      res.status(500).json({
        msq: "No se pudo obtener las cuentas",
        datos: err,
      });
    }
  }

  async dar_de_baja_cuenta(req, res) {
    const cuenta = await Cuenta.findByIdAndUpdate(
      req.params.id,
      { estado: false },
      { new: true }
    );
    res.status(200).json(cuenta);
  }
}

module.exports = CuentaControl;
