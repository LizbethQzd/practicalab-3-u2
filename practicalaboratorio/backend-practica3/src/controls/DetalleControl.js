const Detalle = require('../models/detalle');

class DetalleControl {
    async crear_detalle(req, res) {
        const detalle = new Detalle(req.body);
        await detalle.save();
        res.status(201).json(detalle);
    }

    async obtener_detalles(req, res) {
        const detalle = await Detalle.find();
        res.status(200).json(detalle);
    }

    async obtener_detalle(req, res) {
        const detalle = await Detalle.findById(req.params.id);
        res.status(200).json(detalle);
    }

    async actualizar_detalle(req, res) {
        const detalle = await Detalle.findByIdAndUpdate(req.params.id, req.body, { new: true });
        res.status(200).json(detalle);
    }

    async eliminar_detalle(req, res) {
        const detalle = await Detalle.findByIdAndDelete(req.params.id);
        res.status(200).json(detalle);
    }
}

module.exports = DetalleControl;