const Pago = require("../models/pago");
const Libro = require("../models/libro");
const Detalle = require("../models/detalle");
const Factura = require("../models/factura");
const Persona = require("../models/persona");
const { Op } = require("sequelize");
const Cuenta = require("../models/cuenta");
var uuid = require("uuid");
var jwt = require("jsonwebtoken");
require("dotenv").config();

class PagoControl {
  async listar_ventas_pendientes(req, res) {
    try {
      const decoded = jwt.verify(
        req.cookies?.mytoken,
        process.env.ACCESS_TOKEN
      );
      const cuenta = await Cuenta.findOne({
        where: { external_id: decoded.info.external },
      });

      if (!cuenta) {
        return res.status(404).json({ msg: "No existe la cuenta" });
      }

      const persona = await Persona.findByPk(cuenta.id_persona);
      if (!persona) {
        return res.status(404).json({ msg: "No existe la persona" });
      }

      const librosComprados = await Libro.findAll({
        where: { id_persona: persona.id },
      });
      if (!librosComprados || librosComprados.length === 0) {
        return res.status(404).json({ msg: "No existen libros comprados" });
      }

      const detallePromises = librosComprados.map(async (libro) => {
        const detalles = await Detalle.findAll({
          where: { id_libro: libro.id },
        });
        return detalles;
      });

      const detalles = await Promise.all(detallePromises);

      const ventasPendientes = detalles
        .flat()
        .filter((detalle) => detalle.id_pago === null);

      if (ventasPendientes.length === 0) {
        return res.status(404).json({ msg: "No existen ventas pendientes" });
      }

      res
        .status(200)
        .json({ libros: librosComprados, detalles: ventasPendientes });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Error al obtener las ventas pendientes" });
    }
  }

  async cliente(req, res) {
    try {
      const external_id = req.params.external_id;
      console.log(external_id);

      const detalle = await Detalle.findOne({
        where: { external_id: external_id },
      });

      if (!detalle) {
        return res.status(404).json({ msg: "No existe el detalle" });
      }

      const factura = await Factura.findOne({
        where: { id: detalle.id_factura },
      });

      if (!factura) {
        return res.status(404).json({ msg: "No existe la factura" });
      }

      const persona = await Persona.findOne({
        where: { id: factura.id_persona },
      });

      if (!persona) {
        return res.status(404).json({ msg: "No existe la persona" });
      }
      res.status(200).json({ persona: persona });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Error al obtener el cliente" });
    }
  }

  async listar_ventas(req, res) {
    try {
      const decoded = jwt.verify(
        req.cookies?.mytoken,
        process.env.ACCESS_TOKEN
      );
      const cuenta = await Cuenta.findOne({
        where: { external_id: decoded.info.external },
      });

      if (!cuenta) {
        return res.status(404).json({ msg: "No existe la cuenta" });
      }

      const persona = await Persona.findByPk(cuenta.id_persona);
      if (!persona) {
        return res.status(404).json({ msg: "No existe la persona" });
      }

      const librosComprados = await Libro.findAll({
        where: { id_persona: persona.id },
      });
      if (!librosComprados || librosComprados.length === 0) {
        return res.status(404).json({ msg: "No existen libros comprados" });
      }

      const detallePromises = librosComprados.map(async (libro) => {
        const detalles = await Detalle.findAll({
          where: { id_libro: libro.id },
        });
        return detalles;
      });

      const detalles = await Promise.all(detallePromises);

      const ventasPendientes = detalles
        .flat()
        .filter((detalle) => detalle.id_pago !== null);

      if (ventasPendientes.length === 0) {
        return res.status(404).json({ msg: "No existen ventas pendientes" });
      }

      res
        .status(200)
        .json({ libros: librosComprados, detalles: ventasPendientes });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Error al obtener las ventas pendientes" });
    }
  }

  async obtener_datos_edicion (req, res) {
    try {
        const external_id = req.params.external_id;
        const detalle = await Detalle.findOne({ where: { external_id: external_id } });
        if (!detalle) {
            return res.status(404).json({ msg: 'No existe el detalle' });
        }

        const factura = await Factura.findOne({ where: { id: detalle.id_factura } });
        if (!factura) {
            return res.status(404).json({ msg: 'No existe la factura' });
        }

        const pago = await Pago.findOne({ where: { id: detalle.id_pago } });
        if (!pago) {
            return res.status(404).json({ msg: 'No existe el pago' });
        }
        res.status(200).json({ tipo: pago.tipo, nombre_compania: factura.nombre_compania, tema_compania: factura.tema_compania, num_factura: factura.num_factura, precio: detalle.precio });
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error al obtener los datos de edición' });
    }
  }

  async modificar_venta(req, res) {
    try {
        const external_id = req.params.external_id;
        const { tipo, nombre_compania, tema_compania, num_factura, precio } = req.body;
        
        const detalle1 = await Detalle.findOne({ where: { external_id: external_id } });
        // Buscar y actualizar el pago
        const pago = await Pago.findOne({ where: { id: detalle1.id_pago  } });
        if (!pago) return res.status(404).json({ msg: 'No existe el pago' });
        
        pago.tipo = tipo;
        await pago.save();
    
        // Buscar y actualizar la factura
        const factura = await Factura.findOne({ where: { id: detalle1.id_factura } });
        if (!factura) return res.status(404).json({ msg: 'No existe la factura' });
    
        factura.nombre_compania = nombre_compania;
        factura.tema_compania = tema_compania;
        factura.num_factura = num_factura;
        await factura.save();
    
        detalle1.precio = precio;
        await detalle1.save();
    
        res.status(200).json({ msg: 'Venta editada exitosamente' });
      } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error al editar la venta' });
      }
  }

  async buscar_venta(req, res) {
    try {
        const { nombreLibro } = req.body;
    
        if (!nombreLibro) {
          return res.status(400).json({ error: 'El nombre del libro es obligatorio.' });
        }
    
        const decoded = jwt.verify(req.cookies?.mytoken, process.env.ACCESS_TOKEN);
        const cuenta = await Cuenta.findOne({ where: { external_id: decoded.info.external } });
    
        if (!cuenta) {
          return res.status(404).json({ msg: 'No existe la cuenta' });
        }
    
        const persona = await Persona.findByPk(cuenta.id_persona);
        if (!persona) {
          return res.status(404).json({ msg: 'No existe la persona' });
        }
    
        const librosComprados = await Libro.findAll({
          where: {
            id_persona: persona.id,
            nombre: {
              [Op.like]: `%${nombreLibro}%`,
            },
          },
        });
    
        if (!librosComprados || librosComprados.length === 0) {
          return res.status(404).json({ msg: 'No existen libros comprados.' });
        }
    
        const detallePromises = librosComprados.map(async (libro) => {
          const detalles = await Detalle.findAll({
            where: {
              id_pago: { [Op.not]: null },
              id_libro: libro.id,
            },
          });
          return detalles;
        });
    
        const ventasPendientes = await Promise.all(detallePromises);
    
        const response = {
          libros: librosComprados,
          detalles: ventasPendientes.flat(),
        };
    
        res.status(200).json(response);
      } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error al buscar ventas por nombre de libro.' });
      }
  }

  async crear_venta(req, res) {
    try {
      const external_id = req.params.external_id;

      const { tipo, nombre_compania, tema_compania, num_factura, precio } =
        req.body;

      const pago = await Pago.create({
        tipo: tipo,
        comprobacion_pago: true,
        external_id: uuid.v4(),
      });

      const decoded = jwt.verify(
        req.cookies?.mytoken,
        process.env.ACCESS_TOKEN
      );
      const cuenta = await Cuenta.findOne({
        where: { external_id: decoded.info.external },
      });
      if (!cuenta) return res.status(404).json({ msg: "No existe la cuenta" });
      const persona = await Persona.findByPk(cuenta.id_persona);
      if (!persona)
        return res.status(404).json({ msg: "No existe la persona" });
      const factura = await Factura.findOne({
        where: { nro_vendedor: persona.external_id },
      });
      if (!factura)
        return res.status(404).json({ msg: "No existe la factura" });

      factura.nombre_compania = nombre_compania;
      factura.tema_compania = tema_compania;
      factura.num_factura = num_factura;
      await factura.save();

      const detalle = await Detalle.findOne({
        where: { external_id: external_id },
      });
      if (!detalle) {
        return res.status(404).json({ msg: "No existe el detalle" });
      }

      detalle.precio = precio;
      detalle.id_pago = pago.id;
      await detalle.save();
      res.status(200).json({ msg: "Venta Creada con exito" });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Error al crear la venta" });
    }
  }

  async comprarLibro(req, res) {
    try {
      const { external_id } = req.body;
      const decoded = jwt.verify(
        req.cookies?.mytoken,
        process.env.ACCESS_TOKEN
      );
      const cuenta = await Cuenta.findOne({
        where: { external_id: decoded.info.external },
      });
      if (!cuenta) return res.status(404).json({ msg: "No existe la cuenta" });
      const persona = await Persona.findByPk(cuenta.id_persona);
      if (!persona)
        return res.status(404).json({ msg: "No existe la persona" });
      const libro = await Libro.findOne({
        where: { external_id: external_id },
      });
      if (!libro) return res.status(404).json({ msg: "No existe el libro" });

      const vendedor = await Persona.findOne({
        where: { id: libro.id_persona },
      });

      libro.num_compras += 1;
      libro.cantidad -= 1;
      await libro.save();

      const nroCliente = "ADF" + cuenta.id;

      const factura = await Factura.create({
        nro_vendedor: vendedor.external_id,
        nro_cliente: nroCliente,
        id_persona: cuenta.id_persona,
        external_id: uuid.v4(),
      });

      const detalle = await Detalle.create({
        id_libro: libro.id,
        id_factura: factura.id,
        cantidad: 1,
        external_id: uuid.v4(),
      });

      res
        .status(200)
        .json({ msg: "Compra exitosa el vendedor tiene que crear la venta" });
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: "Error al comprar el libro" });
    }
  }

}

module.exports = PagoControl;
