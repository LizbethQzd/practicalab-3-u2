require('dotenv').config();
const express = require('express');
const conexion = require('../src/config/conexion');
const cors = require('cors');
const cookieParser = require('cookie-parser')
const app = express();

const PORT = process.env.PORT || 4000;

app.use(require('./middlewares/originCredentials'))
app.use(cors(require('./config/origins')));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cookieParser());
app.use(require('./routes/cuentas'));
app.use(require('./routes/libros'));
app.use(require('./routes/pago'));

conexion();

app.listen(PORT, () => {
  console.log(`Servidor levantado con exito en el puerto ${PORT}!`);
});