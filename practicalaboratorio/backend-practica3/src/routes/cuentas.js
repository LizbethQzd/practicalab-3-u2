const { Router } = require('express');
const router = Router();

const cuentaCon = require('../controls/CuentaControl');
let cuentaControl = new cuentaCon();

router.post('/register', cuentaControl.registro_usuario);
router.post('/register/vendedores', cuentaControl.registro_usuario_por_gerente);
router.post('/login', cuentaControl.inicio_sesion);
router.get('/cuentas', cuentaControl.obtener_cuentas);
router.get('/cuenta/perfil', cuentaControl.obtener_perfil);
router.post('/cerrarsesion', cuentaControl.cerrar_session);
router.get('/obtenerRol', cuentaControl.obtener_rol);
router.get('/obtenervendedores', cuentaControl.obtener_vendedores);

module.exports = router;