const { Router } = require('express');
const router = Router();

const pagoCon = require('../controls/PagoControl');
let pagoControl = new pagoCon();

router.post('/comprarlibro', pagoControl.comprarLibro);

router.get('/ventaspendientes', pagoControl.listar_ventas_pendientes);

router.get('/cliente/:external_id', pagoControl.cliente);

router.post('/crearventa/:external_id', pagoControl.crear_venta);

router.get('/listarventas', pagoControl.listar_ventas);

router.post('/buscarventas', pagoControl.buscar_venta);

router.get('/obtenerdatosventa/:external_id', pagoControl.obtener_datos_edicion)

router.put('/modificarventa/:external_id', pagoControl.modificar_venta);

module.exports = router;