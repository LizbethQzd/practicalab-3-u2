const { Router } = require('express');
const router = Router();
const upload = require('../middlewares/multer');
const uploadA = require('../middlewares/multerA');
const librosCon = require('../controls/LibroControl');
let libroControl = new librosCon();

router.post('/subirimagen', upload.single('image'), libroControl.subirImagen)

router.post('/subirlibro', libroControl.subir_libro);

router.post('/subiraudio', upload.single('audio'),libroControl.subirAudio);

router.get('/obtenerLibros', libroControl.obtener_libros);

router.get('/librovendedor', libroControl.libro_vendedor);

router.get('/obtenerlibro/:external_id', libroControl.obtener_libro_editar);

router.put('/editarLibro/:external_id', libroControl.editar_libro);

router.get('/listarLibros', libroControl.listar_libros);

router.get('/libroscomprados', libroControl.libros_comprados);

router.get('/visualizarquincena', libroControl.visualizar_quincena);

module.exports = router;