const sequelize = require('../config/baseDeDatos');
const { DataTypes } = require('sequelize');

const rol = sequelize.define("rol",{
    tipo: {
        type: DataTypes.STRING(20),
        allowNull: false
    },
    external_id: {
        type: DataTypes.UUID, 
        defaultValue: DataTypes.UUIDV4
    },
},
{
    tableName: 'rol',
    timestamps: true,
    underscored: true,
    sequelize,
}


);


module.exports = rol;