const sequelize = require('../config/baseDeDatos');
const { DataTypes } = require('sequelize');
const Factura = require('./factura');
const Pago = require('./pago');
const Libro = require('./libro');

const Detalle = sequelize.define("detalle",{
    cantidad: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    precio: {
        type: DataTypes.FLOAT,
        allowNull: true
    },
    id_pago: {
        type: DataTypes.INTEGER,
        allowNull: true
    },
    id_factura: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    id_libro: { 
        type: DataTypes.INTEGER,
        allowNull: false
        },
    external_id: {
        type: DataTypes.UUID, 
        defaultValue: DataTypes.UUIDV4}
},
{
    tableName: 'detalle',
    timestamps: true,
    underscored: true,
    sequelize,
}
);

Detalle.belongsTo(Pago, { foreignKey: 'id_pago' });
Pago.hasOne(Detalle, { foreignKey: 'id_pago' });

Detalle.belongsTo(Factura, { foreignKey: 'id_factura' });
Factura.hasMany(Detalle, { foreignKey: 'id_factura' });

Detalle.belongsTo(Libro, { foreignKey: 'id_libro' });
Libro.hasMany(Detalle, { foreignKey: 'id_libro' });


module.exports = Detalle;