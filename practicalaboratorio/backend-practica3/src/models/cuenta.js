const sequelize = require('../config/baseDeDatos');
const { DataTypes } = require('sequelize');
const Persona = require('./persona');

const Cuenta = sequelize.define("cuenta",{
    nombre_usuario: {
        type: DataTypes.STRING(150),
        allowNull: false
    },
    correo: {
        type: DataTypes.STRING(150),
        allowNull: false
    },
    clave: {
        type: DataTypes.STRING,
        allowNull: false
    },
    estado: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true
    },
    id_persona: {
        type: DataTypes.INTEGER,
        allowNull: false,
        unique: true
    },
    external_id: {
        type: DataTypes.UUID, 
        defaultValue: DataTypes.UUIDV4}
},
{
    tableName: 'cuenta',
    timestamps: true,
    underscored: true,
    sequelize,
}
);

Cuenta.belongsTo(Persona, {foreignKey: 'id_persona'});
Persona.hasOne(Cuenta, { foreignKey: 'id_persona' });

module.exports = Cuenta;