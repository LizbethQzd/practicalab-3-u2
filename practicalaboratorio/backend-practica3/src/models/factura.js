const sequelize = require('../config/baseDeDatos');
const { DataTypes } = require('sequelize');
const Persona = require('./persona');

const Factura = sequelize.define("factura",{
    nombre_compania: {
        type: DataTypes.STRING(150),
        allowNull: true,
    },
    tema_compania: {
        type: DataTypes.STRING(150),
        allowNull: true,
    },
    nro_vendedor: {
        type: DataTypes.UUID,
        allowNull: true
    },
    nro_cliente: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    num_factura: {
        type: DataTypes.INTEGER,
        allowNull: true,
    },
    id_persona: { 
        type: DataTypes.INTEGER,
        allowNull: true
        },
    external_id: {
        type: DataTypes.UUID, 
        defaultValue: DataTypes.UUIDV4}
},
{
    tableName: 'factura',
    timestamps: true,
    underscored: true,
    sequelize,
}
);

Factura.belongsTo(Persona, { foreignKey: 'id_persona' });
Persona.hasMany(Factura, { foreignKey: 'id_persona' });

module.exports = Factura;