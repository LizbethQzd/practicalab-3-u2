const sequelize = require('../config/baseDeDatos');
const { DataTypes } = require('sequelize');
const Rol = require('./rol');

const Persona = sequelize.define("persona",{
    nombres: {
        type: DataTypes.STRING(150),
        allowNull: false
    },
    apellidos: {
        type: DataTypes.STRING(150),
        allowNull: false
    },
    ruc_ci: {
        type: DataTypes.STRING(15),
        allowNull: false
    },
    direccion: {
        type: DataTypes.STRING,
        allowNull: false
    },
    celular: {
        type: DataTypes.STRING(20),
        allowNull: false
    },
    fecha_nacimiento: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    dar_de_baja: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    },
    id_rol: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    external_id: {
        type: DataTypes.UUID, 
        defaultValue: DataTypes.UUIDV4}
},
{
    tableName: 'persona',
    timestamps: true,
    underscored: true,
    sequelize,
}
);

Persona.belongsTo(Rol, { foreignKey: "id_rol" });
Rol.hasMany(Persona, { foreignKey: "id_rol" });

module.exports = Persona;