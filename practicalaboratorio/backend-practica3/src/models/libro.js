const sequelize = require("../config/baseDeDatos");
const { DataTypes } = require("sequelize");
const Persona = require("./persona");

const Libro = sequelize.define(
  "libro",
  {
    nombre: {
      type: DataTypes.STRING(150),
      allowNull: false,
    },
    codigo: {
      type: DataTypes.STRING(150),
      allowNull: false,
    },
    detalle: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    imagen: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    audio: { 
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null
    },
    precio: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0,
    },
    cantidad: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    num_compras: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    tipo_libro:{
      type: DataTypes.ENUM('Libro', 'AudioLibro'),
      allowNull: false,
      defaultValue: 'Libro',
    },
    id_persona: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    dar_de_baja: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    external_id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
    },
  },
  {
    tableName: "libro",
    timestamps: true,
    underscored: true,
    sequelize,
  }
);

Libro.belongsTo(Persona, { foreignKey: "id_persona" });
Persona.hasMany(Libro, { foreignKey: "id_persona" });

module.exports = Libro;
