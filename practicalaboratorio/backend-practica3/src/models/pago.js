const sequelize = require('../config/baseDeDatos');
const { DataTypes } = require('sequelize');

const Pago = sequelize.define("pago",{
    tipo: {
        type: DataTypes.STRING,
        allowNull: false
    },
    comprobacion_pago: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    },
    external_id: {
        type: DataTypes.UUID, 
        defaultValue: DataTypes.UUIDV4
    },
},
{
    tableName: 'pago',
    timestamps: true,
    underscored: true,
    sequelize,
}
);

module.exports = Pago;