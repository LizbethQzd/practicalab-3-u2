const bd = require('./baseDeDatos');

require('../models/persona.js');
require('../models/cuenta.js');
require('../models/rol.js');
require('../models/factura.js');
require('../models/pago.js');
require('../models/libro.js');
require('../models/detalle.js');

const conexion = async () => {
    try {
      await bd.authenticate();
      await bd.sync({ force: false });
      console.log('Conexion exitosa a bd');
    } catch (error) {
      console.log(error);
    }
  }
  
module.exports = conexion;