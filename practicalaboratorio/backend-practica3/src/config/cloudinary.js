const cloudinary = require('cloudinary')
require("dotenv").config();
cloudinary.v2.config({
    cloud_name: process.env.NAME_CLOUDINARY,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET,
    secure: true,
  });

module.exports = cloudinary