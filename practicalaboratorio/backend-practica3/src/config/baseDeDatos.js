require('dotenv').config();
const Sequealize = require('sequelize');


const sequelize = new Sequealize(process.env.DATABASE_URL, process.env.USER_DB, process.env.PASSW_DB, {
  host: process.env.HOST_DB,
  dialect: process.env.DIALECT_DB,
  logging: false,
});

module.exports = sequelize